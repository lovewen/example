package org.wen.example.domain.wrapper;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-16 15:27:47
 * @description
 * <p>
 *     统一响应数据实体
 * </p>
 */
public class ResponseWrapper extends HashMap<String, Object>
{
    /** 状态码 */
    public static final String CODE_TAG = "code";
    /** 返回内容 */
    public static final String MSG_TAG = "msg";
    /** 数据对象 */
    public static final String DATA_TAG = "data";
    /** 分页总数 */
    public static final String TOTAL_TAG = "total";
    /** 分页数据列表 */
    public static final String ROWS_TAG = "rows";

    /**
     * 初始化一个新创建的 ResponseWrapper 对象，使其表示一个空消息。
     */
    private ResponseWrapper()
    {
    }

    /**
     * 初始化一个新创建的 ResponseWrapper 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     */
    private ResponseWrapper(int code, String msg)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个新创建的 ResponseWrapper 对象
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @param data 数据对象
     */
    private ResponseWrapper(int code, String msg, Object data)
    {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (data != null)
        {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 初始化一个支持分页的对象
     * @param data 数据对象列表
     */
    private ResponseWrapper(String msg, List<?> data)
    {
        int size = 0;
        super.put(CODE_TAG, HttpStatus.OK.value());
        super.put(MSG_TAG, msg);
        if (data != null)
            size = data.size();
        super.put(TOTAL_TAG, size);
        super.put(ROWS_TAG, data);
    }

    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static ResponseWrapper success()
    {
        return ResponseWrapper.success("操作成功");
    }

    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public static ResponseWrapper success(Object data)
    {
        return ResponseWrapper.success("操作成功", data);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @return 成功消息
     */
    public static ResponseWrapper success(String msg)
    {
        return ResponseWrapper.success(msg, null);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static ResponseWrapper success(String msg, Object data)
    {
        return new ResponseWrapper(HttpStatus.OK.value(), msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @return
     */
    public static ResponseWrapper error()
    {
        return ResponseWrapper.error("操作失败");
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResponseWrapper error(String msg)
    {
        return ResponseWrapper.error(msg, null);
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static ResponseWrapper error(String msg, Object data)
    {
        return new ResponseWrapper(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResponseWrapper error(int code, String msg)
    {
        return new ResponseWrapper(code, msg, null);
    }

    /**
     * 方便链式调用
     *
     * @param key 键
     * @param value 值
     * @return 数据对象
     */
    @Override
    public ResponseWrapper put(String key, Object value)
    {
        super.put(key, value);
        return this;
    }

    /**
     * 返回分页信息
     *
     * @param msg 返回内容
     * @param data 数据对象列表
     * @return 成功消息
     */
    public static ResponseWrapper successRows(String msg, List<?> data)
    {
        return new ResponseWrapper(msg, data);
    }

    /**
     * 返回分页信息
     *
     * @param data 数据对象列表
     * @return 成功消息
     */
    public static ResponseWrapper successRows(List<?> data)
    {
        return new ResponseWrapper("查询成功", data);
    }

}
