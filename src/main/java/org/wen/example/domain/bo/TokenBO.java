package org.wen.example.domain.bo;

import org.wen.example.domain.entity.User;

import java.util.Date;

/**
 * @author loveweni
 * @organization: FOXCONN
 * @create 2022-12-30 17:11:05
 * @description description
 */
public class TokenBO {
    /** token */
    private String token;
    /** 登录人员信息 */
    private User user;
    /** 登录时间 */
    private Date loginTime;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    @Override
    public String toString() {
        return "TokenBO{" +
                "token='" + token + '\'' +
                ", user=" + user +
                ", loginTime=" + loginTime +
                '}';
    }
}
