package org.wen.example.domain.entity;

/**
 * 角色和菜单关联对象 role_permission
 * 
 * @author lovewnei
 * @date 2022-12-30
 */
public class RolePermission
{
    /** 角色ID */
    private Long roleId;

    /** 菜单ID */
    private Long powerId;

    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }
    public void setPowerId(Long powerId) 
    {
        this.powerId = powerId;
    }

    public Long getPowerId() 
    {
        return powerId;
    }

    @Override
    public String toString() {
        return "RolePermission{" +
                "roleId=" + roleId +
                ", powerId=" + powerId +
                '}';
    }
}
