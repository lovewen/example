package org.wen.example.domain.entity;

/**
 * 用户和角色关联对象 user_role
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public class UserRole
{
    /** 用户ID */
    private Long userId;

    /** 角色ID */
    private Long roleId;

    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setRoleId(Long roleId) 
    {
        this.roleId = roleId;
    }

    public Long getRoleId() 
    {
        return roleId;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }
}
