package org.wen.example.domain.entity;

import org.wen.example.domain.base.BaseEntity;

/**
 * 权限对象 permission
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public class Permission extends BaseEntity
{
    /** 权限ID */
    private Long powerId;

    /** 权限key */
    private String powerKey;

    /** 权限名称 */
    private String powerName;

    /** 是否删除 [ 1: 否，2：是 ] */
    private String delFlag;

    public void setPowerId(Long powerId) 
    {
        this.powerId = powerId;
    }

    public Long getPowerId() 
    {
        return powerId;
    }
    public void setPowerKey(String powerKey) 
    {
        this.powerKey = powerKey;
    }

    public String getPowerKey() 
    {
        return powerKey;
    }
    public void setPowerName(String powerName) 
    {
        this.powerName = powerName;
    }

    public String getPowerName() 
    {
        return powerName;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "powerId=" + powerId +
                ", powerKey='" + powerKey + '\'' +
                ", powerName='" + powerName + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }
}
