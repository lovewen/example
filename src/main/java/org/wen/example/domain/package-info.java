/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-18 20:54:22
 * @description 领域模型对象包
 * <p>
 *     bo:        业务对象包
 *     base:      父类包
 *     entity包:  实体类包
 *     wrapper包: 统一返回的数据封装对象适用于controller返回，对外api返回;
 * </p>
 */
package org.wen.example.domain;