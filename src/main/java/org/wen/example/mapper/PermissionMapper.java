package org.wen.example.mapper;

import org.wen.example.domain.entity.Permission;

import java.util.List;

/**
 * 权限Mapper接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface PermissionMapper 
{
    /**
     * 查询权限
     * 
     * @param powerId 权限主键
     * @return 权限
     */
    public Permission selectPermissionByPowerId(Long powerId);

    /**
     * 查询权限列表
     * 
     * @param permission 权限
     * @return 权限集合
     */
    public List<Permission> selectPermissionList(Permission permission);

    /**
     * 新增权限
     * 
     * @param permission 权限
     * @return 结果
     */
    public int insertPermission(Permission permission);

    /**
     * 修改权限
     * 
     * @param permission 权限
     * @return 结果
     */
    public int updatePermission(Permission permission);

    /**
     * 删除权限
     * 
     * @param powerId 权限主键
     * @return 结果
     */
    public int deletePermissionByPowerId(Long powerId);

    /**
     * 批量删除权限
     * 
     * @param powerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePermissionByPowerIds(String[] powerIds);
}
