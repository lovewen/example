package org.wen.example.mapper;

import java.util.List;
import org.wen.example.domain.entity.Role;

/**
 * 角色信息Mapper接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface RoleMapper 
{
    /**
     * 查询角色信息
     * 
     * @param roleId 角色信息主键
     * @return 角色信息
     */
    public Role selectRoleByRoleId(Long roleId);

    /**
     * 查询角色信息列表
     * 
     * @param role 角色信息
     * @return 角色信息集合
     */
    public List<Role> selectRoleList(Role role);

    /**
     * 新增角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(Role role);

    /**
     * 修改角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(Role role);

    /**
     * 删除角色信息
     * 
     * @param roleId 角色信息主键
     * @return 结果
     */
    public int deleteRoleByRoleId(Long roleId);

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRoleByRoleIds(String[] roleIds);
}
