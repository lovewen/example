package org.wen.example.mapper;

import java.util.List;
import org.wen.example.domain.entity.RolePermission;

/**
 * 角色和权限关联Mapper接口
 * 
 * @author lovewnei
 * @date 2022-12-30
 */
public interface RolePermissionMapper 
{
    /**
     * 查询角色和权限关联
     * 
     * @param roleId 角色和权限关联主键
     * @return 角色和权限关联
     */
    public RolePermission selectRolePermissionByRoleId(Long roleId);

    /**
     * 查询角色和权限关联列表
     * 
     * @param rolePermission 角色和权限关联
     * @return 角色和权限关联集合
     */
    public List<RolePermission> selectRolePermissionList(RolePermission rolePermission);

    /**
     * 新增角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    public int insertRolePermission(RolePermission rolePermission);

    /**
     * 修改角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    public int updateRolePermission(RolePermission rolePermission);

    /**
     * 删除角色和权限关联
     * 
     * @param roleId 角色和权限关联主键
     * @return 结果
     */
    public int deleteRolePermissionByRoleId(Long roleId);

    /**
     * 批量删除角色和权限关联
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRolePermissionByRoleIds(String[] roleIds);
}
