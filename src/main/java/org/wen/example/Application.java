package org.wen.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-30 08:50:18
 * @description 启动类
 */
// 全局开启定时任务功能
@EnableScheduling
@SpringBootApplication
@MapperScan("org.wen.example.**.mapper")
public class Application {
    public static void main(String[] args) {
        // 运行应用 传参[ 运行时类，args参数 ]
        SpringApplication.run(Application.class, args);
        System.out.println
                (
                "————————————————————————————————————————————————————————————————————————————\n" +
                "Application is run\n" +
                "————————————————————————————————————————————————————————————————————————————"
                );
    }
}
