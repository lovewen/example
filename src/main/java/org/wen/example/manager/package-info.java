/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-18 20:46:24
 * @description
 * <p>
 *     aop: 切面编程包
 *     config: 配置包
 *     enums: 枚举包
 *     exception: 异常包
 *     scheduler: 定时任务包
 *     tool: 工具类包
 *
 *     技术服务管理层
 *     对整个应用的技术支撑
 * </p>
 */
package org.wen.example.manager;