package org.wen.example.manager.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-16 10:07:19
 * @description
 * <p>
 *     翻译数据调度程序
 * </p>
 */
@Component
public class TranslateDataTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(TranslateDataTask.class);

    /**
     * cron: {秒数} {分钟} {小时} {日期} {月份} {星期} {年份(可为空)}
     */
    @Scheduled(cron = "0/60 * * * * ?")
    public void translateTask()
    {

    }
}