package org.wen.example.manager.aop;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-15 14:05:27
 * @description
 * <p>
 *     返回结果切面处理类
 * </p>
 */
@Aspect
@Component
public class ResponseAspect {

    /**
     * <p>
     *     有两个表达式：
     *       1. annotation(注解的全限定类名)
     *       2. execution()
     *            *: 表示通配符
     *            ..: 表示当前包及其下的任何包 [ 在参数中表示: 任何参数 ]
     *            execution(返回值 包路径..方法名(参数))
     * </p>
     */
    @Pointcut("execution(* org.wen.example.controller..*.*(..))")
    private void executionPointcut(){}

    @Pointcut("@annotation(org.wen.example.manager.aop.ResponseAdvice)")
    private void annotationPointcut(){}

}
