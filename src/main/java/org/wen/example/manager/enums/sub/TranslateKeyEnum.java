package org.wen.example.manager.enums.sub;

import org.wen.example.manager.enums.BaseEnum;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-25 19:52:39
 * @description 百度翻译key
 */
public enum  TranslateKeyEnum implements BaseEnum {

    WU_APP_KEY(-500, "20220119001059223"),
    WU_APP_SECRET(-501, "q8Q_fMh6xNwy21lAb0YF"),
    ;

    private final int code;
    private final String desc;

    TranslateKeyEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
