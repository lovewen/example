package org.wen.example.manager.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-25 19:39:36
 * @description
 * <p>
 *     基类枚举
 * </p>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public interface BaseEnum {
    public int getCode();
    public String getDesc();
}
