package org.wen.example.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-17 08:24:53
 * @description
 * <P>
 *     远程调用方法封装
 *       配置RestTemplate [ 可以支持调用http、https协议的api ]
 * </P>
 */

@Configuration
public class RestTemplateConfig {
    /**
     * 支持https调用
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(30 * 1000);
        factory.setReadTimeout(60 * 1000);
        return new RestTemplate(factory);
    }
}