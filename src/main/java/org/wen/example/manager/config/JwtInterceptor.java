package org.wen.example.manager.config;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.wen.example.domain.bo.TokenBO;
import org.wen.example.domain.entity.User;
import org.wen.example.manager.exception.sub.AppException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-16 11:25:42
 * @description
 * <p>
 *     定义过滤连
 * </p>
 */
@Component
public class JwtInterceptor implements HandlerInterceptor {

    private static final String TOKEN = "Authorization";

    // 简单容器简介 <token, <时间, 登录人员信息>>
    public static final Map<String, TokenBO> tokenData = new HashMap<>();

    /**
     * api前拦截
     * @param request
     * @param response
     * @param handler
     * @return
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){

        // 获取请求头里的token
        String token = request.getHeader(TOKEN);

        // 未找到则让其去登录
        if (StrUtil.isEmpty(token)){
            throw new AppException("请先登录系统！！！");
        }

        // 校验token是否存在
        if (tokenData.containsKey(token)){
            throw new AppException("验证信息失败，请重新登录！！！");
        }

        // 校验token是否已过期
        Date loginTime = tokenData.get(token).getLoginTime();
        if (DateUtil.between(loginTime, DateUtil.date(), DateUnit.MINUTE) > 30){
            // 删除已过期的token
            tokenData.remove(token);
            throw new AppException("token已过期，请重新登录！！！");
        }

        // 验证通过刷新token有效期
        TokenBO tokenBO = tokenData.get(token);
        tokenBO.setLoginTime(DateUtil.date());
        tokenData.put(token, tokenBO);
        return true;
    }

    /**
     * api处理后拦截
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     */
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView){
        System.out.println("---------------------------  对响应体修改  ---------------------------");
    }

    /**
     * 请求完成后、返回客户端前拦截
     * @param request
     * @param response
     * @param handler
     * @param ex
     */
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex){
        System.out.println("---------------------------  环绕拦截  ---------------------------");
    }
}