package org.wen.example.manager.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author loveweni
 * @organization: FOXCONN
 * @create 2022-12-16 13:48:26
 * @description description
 */
@Component
public class TestInterceptor implements HandlerInterceptor {
    /**
     * api前拦截
     * @param request
     * @param response
     * @param handler
     * @return
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        System.out.println("---------------------------  通过test校验  ---------------------------");
        return true;
    }
}
