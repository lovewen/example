package org.wen.example.manager.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-16 11:13:44
 * @description
 * <p>
 *     web应用配置中心
 * </p>
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    // 注入自定义拦截器
    @Autowired
    private JwtInterceptor jwtInterceptor;

    /**
     * 资源拦截器
     * @param registry [ 一个方法中可设置多次且不覆写 ]
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加自定义拦截器到过滤器链上
        registry.addInterceptor(jwtInterceptor)
                // 全部拦截
                .addPathPatterns("/**")
                // 放行地址
                .excludePathPatterns("/login", "/logout");
    }

    /**
     * 跨域配置
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                // 允许访问的主机
                .allowedOrigins("*")
                // 允许访问的方法
                .allowedMethods("GET","POST")
                // 允许访问的请求头
                .allowedHeaders("*")
                .maxAge(3600)
                .allowCredentials(true);
    }
}