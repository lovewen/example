package org.wen.example.manager.exception.sub;

/**
 * @author loveweni
 * @organization: FOXCONN
 * @create 2022-12-16 15:36:50
 * @description
 * <p>
 *     自定义异常
 * </p>
 */
public class AppException extends RuntimeException{

    private int code;
    private String message;

    public AppException(String msg) {
        super(msg);
        this.code = 500;
    }
}
