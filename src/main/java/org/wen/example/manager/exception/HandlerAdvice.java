package org.wen.example.manager.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.manager.exception.sub.AppException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-16 15:27:47
 * @description
 * <p>
 *     统一处理api异常
 * </p>
 */
@RestControllerAdvice
public class HandlerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(HandlerAdvice.class);

    /**
     * 处理特定异常
     * @param e [ 特定异常对象 ]
     * @param request
     */
    @ExceptionHandler(AppException.class)
    public ResponseWrapper handleAppException(AppException e, HttpServletRequest request) {
        LOGGER.error("服务异常，异常参数：{}", e.getMessage());
        return ResponseWrapper.error(500, "服务异常");
    }
}
