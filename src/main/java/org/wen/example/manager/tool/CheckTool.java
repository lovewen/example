package org.wen.example.manager.tool;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-30 14:22:58
 * @description 校验参数工具类
 */
public class CheckTool {

    /**
     * 校验存、删、更新数据对象返回值校验
     * @param result [ 返回数量 ]
     * @return
     */
    public static boolean checkResult(int result){
        return result > 0 ? true : false;
    }

    /**
     * 校验存、删、更新数据列表返回值校验
     * @param result [ 返回数量 ]
     * @param size [ 列表数量 ]
     * @return
     */
    public static boolean checkResult(int result, int size){
        return result == size ? true : false;
    }

}
