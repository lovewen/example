package org.wen.example.service;

import java.util.List;
import org.wen.example.domain.entity.User;

/**
 * 用户信息Service接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface IUserService 
{
    /**
     * 查询用户信息
     * 
     * @param userId 用户信息主键
     * @return 用户信息
     */
    public User selectUserByUserId(Long userId);

    /**
     * 查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    public User selectUserByUserName(String username);

    /**
     * 查询用户信息列表
     * 
     * @param user 用户信息
     * @return 用户信息集合
     */
    public List<User> selectUserList(User user);

    /**
     * 新增用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public boolean insertUser(User user);

    /**
     * 修改用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public boolean updateUser(User user);

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户信息主键集合
     * @return 结果
     */
    public boolean deleteUserByUserIds(String userIds);

    /**
     * 删除用户信息信息
     * 
     * @param userId 用户信息主键
     * @return 结果
     */
    public boolean deleteUserByUserId(Long userId);
}
