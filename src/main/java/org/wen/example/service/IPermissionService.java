package org.wen.example.service;

import java.util.List;
import org.wen.example.domain.entity.Permission;

/**
 * 权限Service接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface IPermissionService 
{
    /**
     * 查询权限
     * 
     * @param powerId 权限主键
     * @return 权限
     */
    public Permission selectPermissionByPowerId(Long powerId);

    /**
     * 查询权限列表
     * 
     * @param permission 权限
     * @return 权限集合
     */
    public List<Permission> selectPermissionList(Permission permission);

    /**
     * 新增权限
     * 
     * @param permission 权限
     * @return 结果
     */
    public boolean insertPermission(Permission permission);

    /**
     * 修改权限
     * 
     * @param permission 权限
     * @return 结果
     */
    public boolean updatePermission(Permission permission);

    /**
     * 批量删除权限
     * 
     * @param powerIds 需要删除的权限主键集合
     * @return 结果
     */
    public boolean deletePermissionByPowerIds(String powerIds);

    /**
     * 删除权限信息
     * 
     * @param powerId 权限主键
     * @return 结果
     */
    public boolean deletePermissionByPowerId(Long powerId);
}
