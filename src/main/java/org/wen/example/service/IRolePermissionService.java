package org.wen.example.service;

import java.util.List;
import org.wen.example.domain.entity.RolePermission;

/**
 * 角色和权限关联Service接口
 * 
 * @author lovewnei
 * @date 2022-12-30
 */
public interface IRolePermissionService 
{
    /**
     * 查询角色和权限关联
     * 
     * @param roleId 角色和权限关联主键
     * @return 角色和权限关联
     */
    public RolePermission selectRolePermissionByRoleId(Long roleId);

    /**
     * 查询角色和权限关联列表
     * 
     * @param rolePermission 角色和权限关联
     * @return 角色和权限关联集合
     */
    public List<RolePermission> selectRolePermissionList(RolePermission rolePermission);

    /**
     * 新增角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    public boolean insertRolePermission(RolePermission rolePermission);

    /**
     * 修改角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    public boolean updateRolePermission(RolePermission rolePermission);

    /**
     * 批量删除角色和权限关联
     * 
     * @param roleIds 需要删除的角色和权限关联主键集合
     * @return 结果
     */
    public boolean deleteRolePermissionByRoleIds(String roleIds);

    /**
     * 删除角色和权限关联信息
     * 
     * @param roleId 角色和权限关联主键
     * @return 结果
     */
    public boolean deleteRolePermissionByRoleId(Long roleId);
}
