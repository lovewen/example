package org.wen.example.service;

import java.util.List;
import org.wen.example.domain.entity.UserRole;

/**
 * 用户和角色关联Service接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface IUserRoleService 
{
    /**
     * 查询用户和角色关联
     * 
     * @param userId 用户和角色关联主键
     * @return 用户和角色关联
     */
    public UserRole selectUserRoleByUserId(Long userId);

    /**
     * 查询用户和角色关联列表
     * 
     * @param userRole 用户和角色关联
     * @return 用户和角色关联集合
     */
    public List<UserRole> selectUserRoleList(UserRole userRole);

    /**
     * 新增用户和角色关联
     * 
     * @param userRole 用户和角色关联
     * @return 结果
     */
    public boolean insertUserRole(UserRole userRole);

    /**
     * 修改用户和角色关联
     * 
     * @param userRole 用户和角色关联
     * @return 结果
     */
    public boolean updateUserRole(UserRole userRole);

    /**
     * 批量删除用户和角色关联
     * 
     * @param userIds 需要删除的用户和角色关联主键集合
     * @return 结果
     */
    public boolean deleteUserRoleByUserIds(String userIds);

    /**
     * 删除用户和角色关联信息
     * 
     * @param userId 用户和角色关联主键
     * @return 结果
     */
    public boolean deleteUserRoleByUserId(Long userId);
}
