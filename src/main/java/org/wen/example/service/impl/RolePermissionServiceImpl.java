package org.wen.example.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wen.example.manager.tool.CheckTool;
import org.wen.example.mapper.RolePermissionMapper;
import org.wen.example.domain.entity.RolePermission;
import org.wen.example.service.IRolePermissionService;

/**
 * 角色和权限关联Service业务层处理
 * 
 * @author lovewnei
 * @date 2022-12-30
 */
@Service
public class RolePermissionServiceImpl implements IRolePermissionService 
{
    @Autowired(required = false)
    private RolePermissionMapper rolePermissionMapper;

    /**
     * 查询角色和权限关联
     * 
     * @param roleId 角色和权限关联主键
     * @return 角色和权限关联
     */
    @Override
    public RolePermission selectRolePermissionByRoleId(Long roleId)
    {
        return rolePermissionMapper.selectRolePermissionByRoleId(roleId);
    }

    /**
     * 查询角色和权限关联列表
     * 
     * @param rolePermission 角色和权限关联
     * @return 角色和权限关联
     */
    @Override
    public List<RolePermission> selectRolePermissionList(RolePermission rolePermission)
    {
        return rolePermissionMapper.selectRolePermissionList(rolePermission);
    }

    /**
     * 新增角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    @Override
    public boolean insertRolePermission(RolePermission rolePermission)
    {
        return CheckTool.checkResult(rolePermissionMapper.insertRolePermission(rolePermission));
    }

    /**
     * 修改角色和权限关联
     * 
     * @param rolePermission 角色和权限关联
     * @return 结果
     */
    @Override
    public boolean updateRolePermission(RolePermission rolePermission)
    {
        return CheckTool.checkResult(rolePermissionMapper.updateRolePermission(rolePermission));
    }

    /**
     * 批量删除角色和权限关联
     * 
     * @param roleIds 需要删除的角色和权限关联主键
     * @return 结果
     */
    @Override
    public boolean deleteRolePermissionByRoleIds(String roleIds)
    {
        String[] split = roleIds.split(",");
        return CheckTool.checkResult(rolePermissionMapper.deleteRolePermissionByRoleIds(split), split.length);
    }

    /**
     * 删除角色和权限关联信息
     * 
     * @param roleId 角色和权限关联主键
     * @return 结果
     */
    @Override
    public boolean deleteRolePermissionByRoleId(Long roleId)
    {
        return CheckTool.checkResult(rolePermissionMapper.deleteRolePermissionByRoleId(roleId));
    }
}
