package org.wen.example.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wen.example.manager.tool.CheckTool;
import org.wen.example.mapper.UserRoleMapper;
import org.wen.example.domain.entity.UserRole;
import org.wen.example.service.IUserRoleService;

/**
 * 用户和角色关联Service业务层处理
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@Service
public class UserRoleServiceImpl implements IUserRoleService 
{
    @Autowired(required = false)
    private UserRoleMapper userRoleMapper;

    /**
     * 查询用户和角色关联
     * 
     * @param userId 用户和角色关联主键
     * @return 用户和角色关联
     */
    @Override
    public UserRole selectUserRoleByUserId(Long userId)
    {
        return userRoleMapper.selectUserRoleByUserId(userId);
    }

    /**
     * 查询用户和角色关联列表
     * 
     * @param userRole 用户和角色关联
     * @return 用户和角色关联
     */
    @Override
    public List<UserRole> selectUserRoleList(UserRole userRole)
    {
        return userRoleMapper.selectUserRoleList(userRole);
    }

    /**
     * 新增用户和角色关联
     * 
     * @param userRole 用户和角色关联
     * @return 结果
     */
    @Override
    public boolean insertUserRole(UserRole userRole)
    {
        return CheckTool.checkResult(userRoleMapper.insertUserRole(userRole));
    }

    /**
     * 修改用户和角色关联
     * 
     * @param userRole 用户和角色关联
     * @return 结果
     */
    @Override
    public boolean updateUserRole(UserRole userRole)
    {
        return CheckTool.checkResult(userRoleMapper.updateUserRole(userRole));
    }

    /**
     * 批量删除用户和角色关联
     * 
     * @param userIds 需要删除的用户和角色关联主键
     * @return 结果
     */
    @Override
    public boolean deleteUserRoleByUserIds(String userIds)
    {
        String[] split = userIds.split(",");
        return CheckTool.checkResult(userRoleMapper.deleteUserRoleByUserIds(split), split.length);
    }

    /**
     * 删除用户和角色关联信息
     * 
     * @param userId 用户和角色关联主键
     * @return 结果
     */
    @Override
    public boolean deleteUserRoleByUserId(Long userId)
    {
        return CheckTool.checkResult(userRoleMapper.deleteUserRoleByUserId(userId));
    }
}
