package org.wen.example.service.impl;

import java.util.List;

import cn.hutool.core.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wen.example.manager.tool.CheckTool;
import org.wen.example.mapper.UserMapper;
import org.wen.example.domain.entity.User;
import org.wen.example.service.IUserService;

/**
 * 用户信息Service业务层处理
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@Service
public class UserServiceImpl implements IUserService 
{
    @Autowired(required = false)
    private UserMapper userMapper;

    /**
     * 查询用户信息
     * 
     * @param userId 用户信息主键
     * @return 用户信息
     */
    @Override
    public User selectUserByUserId(Long userId)
    {
        return userMapper.selectUserByUserId(userId);
    }

    /**
     * 查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    public User selectUserByUserName(String username)
    {
        return userMapper.selectUserByUserName(username);
    }

    /**
     * 查询用户信息列表
     * 
     * @param user 用户信息
     * @return 用户信息
     */
    @Override
    public List<User> selectUserList(User user)
    {
        return userMapper.selectUserList(user);
    }

    /**
     * 新增用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean insertUser(User user)
    {
        user.setCreateTime(DateUtil.date());
        return CheckTool.checkResult(userMapper.insertUser(user));
    }

    /**
     * 修改用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean updateUser(User user)
    {
        user.setUpdateTime(DateUtil.date());
        return CheckTool.checkResult(userMapper.updateUser(user));
    }

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public boolean deleteUserByUserIds(String userIds)
    {
        String[] split = userIds.split(",");
        return CheckTool.checkResult(userMapper.deleteUserByUserIds(split), split.length);
    }

    /**
     * 删除用户信息信息
     * 
     * @param userId 用户信息主键
     * @return 结果
     */
    @Override
    public boolean deleteUserByUserId(Long userId)
    {
        return CheckTool.checkResult(userMapper.deleteUserByUserId(userId));
    }
}
