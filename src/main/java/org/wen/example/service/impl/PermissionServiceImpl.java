package org.wen.example.service.impl;

import java.util.List;

import cn.hutool.core.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wen.example.manager.tool.CheckTool;
import org.wen.example.mapper.PermissionMapper;
import org.wen.example.domain.entity.Permission;
import org.wen.example.service.IPermissionService;

/**
 * 权限Service业务层处理
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@Service
public class PermissionServiceImpl implements IPermissionService 
{
    @Autowired(required = false)
    private PermissionMapper permissionMapper;

    /**
     * 查询权限
     * 
     * @param powerId 权限主键
     * @return 权限
     */
    @Override
    public Permission selectPermissionByPowerId(Long powerId)
    {
        return permissionMapper.selectPermissionByPowerId(powerId);
    }

    /**
     * 查询权限列表
     * 
     * @param permission 权限
     * @return 权限
     */
    @Override
    public List<Permission> selectPermissionList(Permission permission)
    {
        return permissionMapper.selectPermissionList(permission);
    }

    /**
     * 新增权限
     * 
     * @param permission 权限
     * @return 结果
     */
    @Override
    public boolean insertPermission(Permission permission)
    {
        // 新增创建时间
        permission.setCreateTime(DateUtil.date());
        // 执行插入
        return CheckTool.checkResult(permissionMapper.insertPermission(permission));
    }

    /**
     * 修改权限
     * 
     * @param permission 权限
     * @return 结果
     */
    @Override
    public boolean updatePermission(Permission permission)
    {
        permission.setUpdateTime(DateUtil.date());
        return CheckTool.checkResult(permissionMapper.updatePermission(permission));
    }

    /**
     * 批量删除权限
     * 
     * @param powerIds 需要删除的权限主键
     * @return 结果
     */
    @Override
    public boolean deletePermissionByPowerIds(String powerIds)
    {
        String[] split = powerIds.split(",");
        if (split == null){

        }
        return CheckTool.checkResult(permissionMapper.deletePermissionByPowerIds(split), split.length);
    }

    /**
     * 删除权限信息
     * 
     * @param powerId 权限主键
     * @return 结果
     */
    @Override
    public boolean deletePermissionByPowerId(Long powerId)
    {
        return CheckTool.checkResult(permissionMapper.deletePermissionByPowerId(powerId));
    }
}
