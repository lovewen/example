package org.wen.example.service.impl;

import java.util.List;

import cn.hutool.core.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wen.example.manager.tool.CheckTool;
import org.wen.example.mapper.RoleMapper;
import org.wen.example.domain.entity.Role;
import org.wen.example.service.IRoleService;

/**
 * 角色信息Service业务层处理
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@Service
public class RoleServiceImpl implements IRoleService 
{
    @Autowired(required = false)
    private RoleMapper roleMapper;

    /**
     * 查询角色信息
     * 
     * @param roleId 角色信息主键
     * @return 角色信息
     */
    @Override
    public Role selectRoleByRoleId(Long roleId)
    {
        return roleMapper.selectRoleByRoleId(roleId);
    }

    /**
     * 查询角色信息列表
     * 
     * @param role 角色信息
     * @return 角色信息
     */
    @Override
    public List<Role> selectRoleList(Role role)
    {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 新增角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public boolean insertRole(Role role)
    {
        role.setCreateTime(DateUtil.date());
        return CheckTool.checkResult(roleMapper.insertRole(role));
    }

    /**
     * 修改角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public boolean updateRole(Role role)
    {
        role.setUpdateTime(DateUtil.date());
        return CheckTool.checkResult(roleMapper.updateRole(role));
    }

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色信息主键
     * @return 结果
     */
    @Override
    public boolean deleteRoleByRoleIds(String roleIds)
    {
        String[] split = roleIds.split(",");
        return CheckTool.checkResult(roleMapper.deleteRoleByRoleIds(split), split.length);
    }

    /**
     * 删除角色信息信息
     * 
     * @param roleId 角色信息主键
     * @return 结果
     */
    @Override
    public boolean deleteRoleByRoleId(Long roleId)
    {
        return CheckTool.checkResult(roleMapper.deleteRoleByRoleId(roleId));
    }
}
