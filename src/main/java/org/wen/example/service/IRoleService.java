package org.wen.example.service;

import java.util.List;
import org.wen.example.domain.entity.Role;

/**
 * 角色信息Service接口
 * 
 * @author loveweni
 * @date 2022-12-30
 */
public interface IRoleService 
{
    /**
     * 查询角色信息
     * 
     * @param roleId 角色信息主键
     * @return 角色信息
     */
    public Role selectRoleByRoleId(Long roleId);

    /**
     * 查询角色信息列表
     * 
     * @param role 角色信息
     * @return 角色信息集合
     */
    public List<Role> selectRoleList(Role role);

    /**
     * 新增角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public boolean insertRole(Role role);

    /**
     * 修改角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public boolean updateRole(Role role);

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色信息主键集合
     * @return 结果
     */
    public boolean deleteRoleByRoleIds(String roleIds);

    /**
     * 删除角色信息信息
     * 
     * @param roleId 角色信息主键
     * @return 结果
     */
    public boolean deleteRoleByRoleId(Long roleId);
}
