package org.wen.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.entity.Role;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.service.IRoleService;

/**
 * 角色信息Controller
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@RestController
@RequestMapping("role")
public class RoleController
{
    @Autowired
    private IRoleService roleService;

    /**
     * 查询角色信息列表
     */
    @PostMapping("query_list")
    @ResponseBody
    public ResponseWrapper queryList(Role role)
    {
        return ResponseWrapper.successRows(roleService.selectRoleList(role));
    }

    /**
     * 查询数据详情
     * @param id [ 数据id ]
     * @return
     */
    @PostMapping("query_info")
    @ResponseBody
    public ResponseWrapper queryInfo(@RequestParam Long id)
    {
        return ResponseWrapper.success("查询详情成功", roleService.selectRoleByRoleId(id));
    }

    /**
     * 新增保存角色信息
     */
    @PostMapping("save")
    @ResponseBody
    public ResponseWrapper save(Role role)
    {
        return roleService.insertRole(role) ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 修改保存角色信息
     */
    @PostMapping("edit")
    @ResponseBody
    public ResponseWrapper edit(Role role)
    {
        return roleService.updateRole(role) ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 删除角色信息
     */
    @PostMapping("remove")
    @ResponseBody
    public ResponseWrapper remove(String ids)
    {
        return roleService.deleteRoleByRoleIds(ids) ? ResponseWrapper.success() : ResponseWrapper.error();
    }
}
