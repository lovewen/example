package org.wen.example.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.bo.TokenBO;
import org.wen.example.domain.entity.User;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.manager.config.JwtInterceptor;
import org.wen.example.manager.exception.sub.AppException;
import org.wen.example.service.IUserService;

/**
 * @author loveweni
 * @organization: FOXCONN
 * @create 2022-12-30 17:19:07
 * @description description
 */
@RestController
@RequestMapping("auth")
public class LoginController {

    @Autowired
    private IUserService userService;

    /**
     * 登录方法
     * @param username [ 用户名 ]
     * @param password [ 密码 ]
     * @return
     */
    @PostMapping("login")
    @ResponseBody
    public ResponseWrapper login(@RequestParam String username, @RequestParam String password){
        // 参数校验 [ 不能为空 且 长度小于30个字符 ]
        if (StrUtil.isEmpty(username) || StrUtil.isEmpty(password) || username.length()<30 || password.length()<30){
            throw new AppException("参数非法，请检查后重新输入");
        }

        // 校验用户名是否存在
        User user = userService.selectUserByUserName(username);
        if (user == null){
            throw new AppException("用户名不存在，请检查后重新输入");
        }

        // 校验密码是否正确
        if (StrUtil.equals(user.getPassword(), password)){
            throw new AppException("密码错误，请检查后重新输入");
        }

        // 生成token, 并存储token和信息
        String token = IdUtil.fastSimpleUUID();
        TokenBO tokenBO = new TokenBO();
        tokenBO.setLoginTime(DateUtil.date());
        tokenBO.setToken(token);
        tokenBO.setUser(user);
        JwtInterceptor.tokenData.put(token, tokenBO);
        return ResponseWrapper.success("登录成功，请携带令牌访问", tokenBO);
    }

}
