package org.wen.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.entity.UserRole;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.service.IUserRoleService;

/**
 * 用户和角色关联Controller
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@RestController
@RequestMapping("user_role")
public class UserRoleController
{

    @Autowired
    private IUserRoleService userRoleService;

    /**
     * 查询用户和角色关联列表
     */
    @PostMapping("query_list")
    @ResponseBody
    public ResponseWrapper queryList(UserRole userRole)
    {
        return ResponseWrapper.successRows(userRoleService.selectUserRoleList(userRole));
    }

    /**
     * 查询数据详情
     * @param id [ 数据id ]
     * @return
     */
    @PostMapping("query_info")
    @ResponseBody
    public ResponseWrapper queryInfo(@RequestParam Long id)
    {
        return ResponseWrapper.success("查询详情成功", userRoleService.selectUserRoleByUserId(id));
    }

    /**
     * 新增保存用户和角色关联
     */
    @PostMapping("save")
    @ResponseBody
    public ResponseWrapper save(UserRole userRole)
    {
        boolean result = userRoleService.insertUserRole(userRole);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 修改保存用户和角色关联
     */
    @PostMapping("edit")
    @ResponseBody
    public ResponseWrapper edit(UserRole userRole)
    {
        boolean result = userRoleService.updateUserRole(userRole);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 删除用户和角色关联
     */
    @PostMapping( "remove")
    @ResponseBody
    public ResponseWrapper remove(String ids)
    {
        boolean result = userRoleService.deleteUserRoleByUserIds(ids);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }
}
