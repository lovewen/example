package org.wen.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.entity.Permission;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.service.IPermissionService;

/**
 * 权限Controller
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@RestController
@RequestMapping("permission")
public class PermissionController
{
    @Autowired
    private IPermissionService permissionService;

    /**
     * 查询权限列表
     */
    @PostMapping("query_list")
    @ResponseBody
    public ResponseWrapper queryList(Permission permission)
    {
        return ResponseWrapper.successRows("查询成功", permissionService.selectPermissionList(permission));
    }

    /**
     * 查询数据详情
     * @param id [ 数据id ]
     * @return
     */
    @PostMapping("query_info")
    @ResponseBody
    public ResponseWrapper queryInfo(@RequestParam Long id)
    {
        return ResponseWrapper.success("查询详情成功", permissionService.selectPermissionByPowerId(id));
    }

    /**
     * 新增权限
     */
    @PostMapping("save")
    @ResponseBody
    public ResponseWrapper save(Permission permission)
    {
        boolean result = permissionService.insertPermission(permission);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }


    /**
     * 修改权限
     */
    @PostMapping("edit")
    @ResponseBody
    public ResponseWrapper edit(Permission permission)
    {
        boolean result = permissionService.updatePermission(permission);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 删除权限
     */
    @PostMapping( "remove")
    @ResponseBody
    public ResponseWrapper remove(String ids)
    {
        boolean result = permissionService.deletePermissionByPowerIds(ids);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }
}
