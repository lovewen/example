package org.wen.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.entity.RolePermission;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.service.IRolePermissionService;

/**
 * 角色和权限关联Controller
 * 
 * @author lovewnei
 * @date 2022-12-30
 */
@RestController
@RequestMapping("role_permission")
public class RolePermissionController
{
    @Autowired
    private IRolePermissionService rolePermissionService;

    /**
     * 查询角色和权限关联列表
     */
    @PostMapping("query_list")
    @ResponseBody
    public ResponseWrapper queryList(RolePermission rolePermission)
    {
        return ResponseWrapper.successRows(rolePermissionService.selectRolePermissionList(rolePermission));
    }

    /**
     * 查询数据详情
     * @param id [ 数据id ]
     * @return
     */
    @PostMapping("query_info")
    @ResponseBody
    public ResponseWrapper queryInfo(@RequestParam Long id)
    {
        return ResponseWrapper.success("查询详情成功", rolePermissionService.selectRolePermissionByRoleId(id));
    }

    /**
     * 新增保存角色和权限关联
     */
    @PostMapping("save")
    @ResponseBody
    public ResponseWrapper save(RolePermission rolePermission)
    {
        boolean result = rolePermissionService.insertRolePermission(rolePermission);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 修改保存角色和权限关联
     */
    @PostMapping("edit")
    @ResponseBody
    public ResponseWrapper edit(RolePermission rolePermission)
    {
        boolean result = rolePermissionService.updateRolePermission(rolePermission);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 删除角色和权限关联
     */
    @PostMapping( "remove")
    @ResponseBody
    public ResponseWrapper remove(String ids)
    {
        boolean result = rolePermissionService.deleteRolePermissionByRoleIds(ids);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }
}
