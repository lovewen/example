package org.wen.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.wen.example.domain.entity.User;
import org.wen.example.domain.wrapper.ResponseWrapper;
import org.wen.example.service.IUserService;

/**
 * 用户信息Controller
 * 
 * @author loveweni
 * @date 2022-12-30
 */
@RestController
@RequestMapping("user")
public class UserController
{
    @Autowired
    private IUserService userService;

    /**
     * 查询用户信息列表
     */
    @PostMapping("query_list")
    @ResponseBody
    public ResponseWrapper queryList(User user)
    {
        return ResponseWrapper.successRows(userService.selectUserList(user));
    }

    /**
     * 查询数据详情
     * @param id [ 数据id ]
     * @return
     */
    @PostMapping("query_info")
    @ResponseBody
    public ResponseWrapper queryInfo(@RequestParam Long id)
    {
        return ResponseWrapper.success("查询详情成功", userService.selectUserByUserId(id));
    }

    /**
     * 新增保存用户信息
     */
    @PostMapping("save")
    @ResponseBody
    public ResponseWrapper save(User user)
    {
        boolean result = userService.insertUser(user);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 修改保存用户信息
     */
    @PostMapping("edit")
    @ResponseBody
    public ResponseWrapper edit(User user)
    {
        boolean result = userService.updateUser(user);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }

    /**
     * 删除用户信息
     */
    @PostMapping( "remove")
    @ResponseBody
    public ResponseWrapper remove(String ids)
    {
        boolean result = userService.deleteUserByUserIds(ids);
        return result ? ResponseWrapper.success() : ResponseWrapper.error();
    }
}
