package org.wen.example;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author loveweni
 * @organization: orange
 * @create 2022-12-14 15:30:25
 * @description 测试
 */
@SpringBootTest
class ApplicationTest {

    /**
     * 测试
     */
    @Test
    void start(){
        System.out.println();
    }
}